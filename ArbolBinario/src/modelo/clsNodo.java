/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author santiago
 */
public class clsNodo {

    private Object dato;
    private clsNodo nodoHijoIzquierda;
    private clsNodo nodoHijoDerecho;

    public clsNodo() {
        this.dato = null;
        this.nodoHijoDerecho = null;
        this.nodoHijoIzquierda = null;
    }

    public clsNodo(Object dato) {
        this.dato = dato;
        this.nodoHijoDerecho = null;
        this.nodoHijoIzquierda = null;

    }

    public Object getDato() {
        return dato;
    }

    public void setDato(Object dato) {
        this.dato = dato;
    }

    public clsNodo getNodoHijoIzquierda() {
        return nodoHijoIzquierda;
    }

    public void setNodoHijoIzquierda(clsNodo nodoHijoIzquierda) {
        this.nodoHijoIzquierda = nodoHijoIzquierda;
    }

    public clsNodo getNodoHijoDerecho() {
        return nodoHijoDerecho;
    }

    public void setNodoHijoDerecho(clsNodo nodoHijoDerecho) {
        this.nodoHijoDerecho = nodoHijoDerecho;
    }
    

}
