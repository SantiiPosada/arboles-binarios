package modelo;

import javax.print.attribute.standard.Finishings;
import javax.swing.JOptionPane;

/**
 *
 * @author santiago
 */
public class clsArbolBinario {

    private clsNodo nodoPadre;

    public clsArbolBinario() {
        this.nodoPadre = null;
    }

    public clsNodo getNodoPadre() {
        return nodoPadre;
    }

    public void setNodoPadre(clsNodo nodoPadre) {
        this.nodoPadre = nodoPadre;
    }

    public void Insercion(int dato) {
        Insercion(nodoPadre, dato);
    }

    private void Insercion(clsNodo Nodo, int dato) {
        if (Nodo == null) {
            clsNodo NuevoNodo = new clsNodo(dato);
            nodoPadre = NuevoNodo;
            return;
        }

        if (dato < (int) Nodo.getDato()) {
            if (Nodo.getNodoHijoIzquierda() == null) {
                clsNodo nuevoNodo = new clsNodo(dato);
                Nodo.setNodoHijoIzquierda(nuevoNodo);
            } else {
                Insercion(Nodo.getNodoHijoIzquierda(), dato);
            }
        } else if (dato > (int) Nodo.getDato()) {
            if (Nodo.getNodoHijoDerecho() == null) {
                clsNodo nuevoNodo = new clsNodo(dato);
                Nodo.setNodoHijoDerecho(nuevoNodo);
            } else {
                Insercion(Nodo.getNodoHijoDerecho(), dato);
            }
        }
    }

    public void recorrdioPreOrden() {

        preOrden(nodoPadre);

    }

    private void preOrden(clsNodo nodo) {

        if (nodo != null) {
            System.out.println(nodo.getDato() + "\t");

            preOrden(nodo.getNodoHijoIzquierda());
            preOrden(nodo.getNodoHijoDerecho());

        }

    }

    public void recorridoInOrden() {
        InOrden(nodoPadre);

    }

    private void InOrden(clsNodo nodo) {

        if (nodo != null) {
            InOrden(nodo.getNodoHijoIzquierda());
            System.out.println(nodo.getDato() + "\t");
            InOrden(nodo.getNodoHijoDerecho());

        }

    }

    public void recorridoPostOrden() {
        PostOrden(nodoPadre);

    }

    private void PostOrden(clsNodo nodo) {

        if (nodo != null) {
            PostOrden(nodo.getNodoHijoIzquierda());
            PostOrden(nodo.getNodoHijoDerecho());
            System.out.println(nodo.getDato() + "\t");

        }

    }

    public boolean ExisteElValor(int valor) {

        return ExisteElValor(valor, nodoPadre);
    }

    private boolean ExisteElValor(int valor, clsNodo nodo) {
        boolean condicion = false;
        if (nodo != null) {
            if (condicion == false) {
                if ((int) nodo.getDato() == valor) {

                    condicion = true;
                    JOptionPane.showMessageDialog(null, "si está " + nodo.getDato());
                    return condicion;
                } else {
                    System.out.println(nodo.getDato() + "\t");

                    boolean izquierda = ExisteElValor(valor, nodo.getNodoHijoIzquierda());

                    if (izquierda == true) {
                        return izquierda;
                    } else {
                        boolean derecha = ExisteElValor(valor, nodo.getNodoHijoDerecho());
                        if (derecha == true) {
                            return derecha;
                        }
                    }

                }
            }

        }

        return condicion;
    }

    public boolean ExisteElValorClase(int valor) {

        return existeValorClase(valor, nodoPadre);
    }

    private boolean existeValorClase(int valor, clsNodo nodo) {
        boolean dato = false;
        if (nodo != null) {
            if (dato == false) {
                if ((int) nodo.getDato() < valor) {
                    boolean menor = existeValorClase(valor, nodo.getNodoHijoIzquierda());
                    if (menor == true) {
                        return menor;

                    } else if ((int) nodo.getDato() > valor) {

                        boolean mayor = existeValorClase(valor, nodo.getNodoHijoDerecho());
                        if (mayor == true) {
                            return mayor;

                        } else if ((int) nodo.getDato() == valor) {

                            JOptionPane.showMessageDialog(null, "si está clase " + nodo.getDato());
                            return true;
                        }

                    }
                }

            }
        }
        return dato;

    }
}