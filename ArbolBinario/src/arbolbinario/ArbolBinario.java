/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbolbinario;

import modelo.clsArbolBinario;

/**
 *
 * @author santiago
 */
public class ArbolBinario {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        clsArbolBinario arbol = new clsArbolBinario();
//23,15,28,17,8,37,2,10,19,35,41,4,12,33,39,3,29,34,40
//23,15,28,17,8,37,2,10,35,41,4,12,33,39,3,29,34,40
//        arbol.Insercion(23);
//        arbol.Insercion(15);
//        arbol.Insercion(28);
//        arbol.Insercion(17);
//        arbol.Insercion(8);
//        arbol.Insercion(37);
//        arbol.Insercion(2);
//        arbol.Insercion(10);
//        arbol.Insercion(19);
//        arbol.Insercion(35);
//        arbol.Insercion(41);
//        arbol.Insercion(4);
//        arbol.Insercion(12);
//        arbol.Insercion(33);
//        arbol.Insercion(39);
//        arbol.Insercion(3);
//        arbol.Insercion(29);
//        arbol.Insercion(34);
//        arbol.Insercion(40);

arbol.Insercion(65);
arbol.Insercion(30);
arbol.Insercion(44);

        //visualgo.net/es/bst
        System.out.println("\n --ÁRBOL RECIEN CREADO--");
        System.out.println("Pre orden");
        arbol.recorrdioPreOrden();

           System.out.println("\n --ÁRBOL RECIEN CREADO--");
        System.out.println("in-orden");
        arbol.recorridoInOrden();
        
             System.out.println("\n --ÁRBOL RECIEN CREADO--");
        System.out.println("post-Orden");
        arbol.recorridoPostOrden();
        
        System.out.println("\n --ÁRBOL RECIEN CREADO--");
        System.out.println("buscar por mi");
        int valor = 30;
        boolean condicion = arbol.ExisteElValor(valor);

        if (condicion == true) {
            System.out.println("Encontró el valor " + valor);
        } else {
            System.out.println("no Encontró el valor " + valor);
        }
        
        
        System.out.println("buscar por calse");
        int valorx = 37;
        boolean condicionx = arbol.ExisteElValorClase(valorx);

        if (condicionx == true) {
            System.out.println("Encontró el valor calse " + valorx);
        } else {
            System.out.println("no Encontró el valor clase " + valorx);
        }

    }

}
