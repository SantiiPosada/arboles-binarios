/*
//... .- -. - .. .- --. --- / .--. --- ... .- -.. .- / .... ..- .-. - .- -.. --- .-.-.
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author santiago
 */
public class clsPersona {
   private int id;
   private String cedula;
   private String nombre;
   private String apellido;
   private String carrera;

    public clsPersona() {
        id=0;
        cedula=null;
        nombre=null;
        apellido=null;
        carrera=null;
        
    }

    public clsPersona(int id, String cedula, String nombre, String apellido, String carrera) {
        this.id = id;
        this.cedula = cedula;
        this.nombre = nombre;
        this.apellido = apellido;
        this.carrera = carrera;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }
   
   
   
   
}
