/*
//... .- -. - .. .- --. --- / .--. --- ... .- -.. .- / .... ..- .-. - .- -.. --- .-.-.
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import javax.swing.JOptionPane;
import modelo.clsNodo;
import modelo.clsPersona;

/**
 *
 * @author santiago
 */
public class ctlArbolBinario {

    String datosPreorden = "";
    String datosInorden = "";
    String datosPostOrden = "";
    private clsNodo nodoPadre;

    public ctlArbolBinario() {
        this.nodoPadre = null;
    }

    public clsNodo getNodoPadre() {
        return nodoPadre;
    }

    public void setNodoPadre(clsNodo nodoPadre) {
        this.nodoPadre = nodoPadre;
    }

    public void Insercion(clsPersona Persona) {
        Insercion(nodoPadre, Persona);
    }

    private void Insercion(clsNodo Nodo, clsPersona Persona) {
        if (Nodo == null) {
            clsNodo NuevoNodo = new clsNodo(Persona);
            nodoPadre = NuevoNodo;
            return;
        }

        if (Persona.getId() < Nodo.getPersona().getId()) {
            if (Nodo.getNodoHijoIzquierda() == null) {
                clsNodo nuevoNodo = new clsNodo(Persona);
                Nodo.setNodoHijoIzquierda(nuevoNodo);
            } else {
                Insercion(Nodo.getNodoHijoIzquierda(), Persona);
            }
        } else if (Persona.getId() > Nodo.getPersona().getId()) {
            if (Nodo.getNodoHijoDerecho() == null) {
                clsNodo nuevoNodo = new clsNodo(Persona);
                Nodo.setNodoHijoDerecho(nuevoNodo);
            } else {
                Insercion(Nodo.getNodoHijoDerecho(), Persona);
            }
        }
    }

    public String recorrdioPreOrden() {
        datosPreorden = "";
        return preOrden(nodoPadre);

    }
//... .- -. - .. .- --. --- / .--. --- ... .- -.. .- / .... ..- .-. - .- -.. --- .-.-.
    private String preOrden(clsNodo nodo) {

        if (nodo != null) {
            datosPreorden += ("Codigo " + nodo.getPersona().getId() + " nombre " + nodo.getPersona().getNombre() + " " + nodo.getPersona().getApellido() + " Cedula " + nodo.getPersona().getCedula() + " Estudia " + nodo.getPersona().getCarrera() + "\n");
            //    System.out.println(nodo.getPersona().getId() + "\t");

            preOrden(nodo.getNodoHijoIzquierda());
            preOrden(nodo.getNodoHijoDerecho());

        }
        return datosPreorden;
    }

    public String recorridoInOrden() {
        datosInorden = "";

        return InOrden(nodoPadre);

    }

    private String InOrden(clsNodo nodo) {

        if (nodo != null) {
            InOrden(nodo.getNodoHijoIzquierda());
            datosInorden += ("Codigo " + nodo.getPersona().getId() + " nombre " + nodo.getPersona().getNombre() + " " + nodo.getPersona().getApellido() + " Cedula " + nodo.getPersona().getCedula() + " Estudia " + nodo.getPersona().getCarrera() + "\n");
            //  System.out.println(nodo.getPersona().getId() + "\t");
            InOrden(nodo.getNodoHijoDerecho());

        }
        return datosInorden;
    }

    public String recorridoPostOrden() {
        datosPostOrden = "";
        return PostOrden(nodoPadre);

    }

    private String PostOrden(clsNodo nodo) {

        if (nodo != null) {
            PostOrden(nodo.getNodoHijoIzquierda());
            PostOrden(nodo.getNodoHijoDerecho());
            //   System.out.println(nodo.getPersona().getId() + "\t");
            datosPostOrden += ("Codigo " + nodo.getPersona().getId() + " nombre " + nodo.getPersona().getNombre() + " " + nodo.getPersona().getApellido() + " Cedula " + nodo.getPersona().getCedula() + " Estudia " + nodo.getPersona().getCarrera() + "\n");
        }
        return datosPostOrden;
    }

    public boolean ExisteElValor(int valor) {

        return ExisteElValor(valor, nodoPadre);
    }

    private boolean ExisteElValor(int valor, clsNodo nodo) {
        boolean condicion = false;
        if (nodo != null) {
            if (condicion == false) {
                if (nodo.getPersona().getId() == valor) {

                    condicion = true;
                    //   imprimir("si está " + nodo.getPersona().getId());
                    return condicion;
                } else {
                    //   System.out.println(nodo.getPersona().getId() + "\t");

                    boolean izquierda = ExisteElValor(valor, nodo.getNodoHijoIzquierda());

                    if (izquierda == true) {
                        return izquierda;
                    } else {
                        boolean derecha = ExisteElValor(valor, nodo.getNodoHijoDerecho());
                        if (derecha == true) {
                            return derecha;
                        }
                    }

                }
            }

        }

        return condicion;
    }

    public boolean ExisteElValorCedula(String valor) {

        return ExisteElValorCedula(valor, nodoPadre);
    }

    private boolean ExisteElValorCedula(String valor, clsNodo nodo) {
        boolean condicion = false;
        if (nodo != null) {
            if (condicion == false) {
                if (nodo.getPersona().getCedula().equals(valor)) {

                    condicion = true;
                    //   imprimir("si está " + nodo.getPersona().getId());
                    return condicion;
                } else {
                    //   System.out.println(nodo.getPersona().getId() + "\t");

                    boolean izquierda = ExisteElValorCedula(valor, nodo.getNodoHijoIzquierda());

                    if (izquierda == true) {
                        return izquierda;
                    } else {
                        boolean derecha = ExisteElValorCedula(valor, nodo.getNodoHijoDerecho());
                        if (derecha == true) {
                            return derecha;
                        }
                    }

                }
            }

        }

        return condicion;
    }

    public clsNodo ObtenerElValor(int valor) {

        return ObtenerElValor(valor, nodoPadre);
    }

    private clsNodo ObtenerElValor(int valor, clsNodo nodo) {
        boolean condicion = false;
        clsNodo persona = new clsNodo();

        if (nodo != null) {
            if (persona.getPersona() == null) {
                if (nodo.getPersona().getId() == valor) {

                    condicion = true;
                    persona = nodo;
                    //   imprimir("si está " + nodo.getPersona().getId());
                    return persona;
                } else {
                    //   System.out.println(nodo.getPersona().getId() + "\t");

                    clsNodo izquierda = ObtenerElValor(valor, nodo.getNodoHijoIzquierda());

                    if (izquierda.getPersona() != null) {
                        return izquierda;
                    } else {
                        clsNodo derecha = ObtenerElValor(valor, nodo.getNodoHijoDerecho());
                        if (derecha.getPersona() != null) {
                            return derecha;
                        }
                    }

                }
            }

        }

        return persona;
    }

    public void EliminarValor(int valor) {

        nodoPadre = EliminarValorNodo(valor, nodoPadre);

    }

    private clsNodo EliminarValorNodo(int valor, clsNodo nodo) {

        if (nodo == null) {

        } else if (valor < nodo.getPersona().getId()) {
            clsNodo izquierdo = EliminarValorNodo(valor, nodo.getNodoHijoIzquierda());

            nodo.setNodoHijoIzquierda(izquierdo);
        } else if (valor > nodo.getPersona().getId()) {
            clsNodo dere = EliminarValorNodo(valor, nodo.getNodoHijoDerecho());
            nodo.setNodoHijoDerecho(dere);
        } else {
            clsNodo nodoAuxiliar = nodo;

            if (nodoAuxiliar.getNodoHijoDerecho() == null) {
                nodo = nodoAuxiliar.getNodoHijoIzquierda();
            } else if (nodoAuxiliar.getNodoHijoIzquierda() == null) {
                nodo = nodoAuxiliar.getNodoHijoDerecho();
            } else {
                nodoAuxiliar = modificar(nodoAuxiliar);
            }
            nodoAuxiliar = null;
        }

        return nodo;
    }

    public boolean ModificarPersona(clsPersona persona, int id) {
        boolean condicion = ExisteElValor(id);
        boolean desicion = false;
        if (condicion == true) {
            EliminarValor(id);
            Insercion(persona);
            desicion = true;
        }
        return desicion;
    }

    private clsNodo modificar(clsNodo nodo) {
        clsNodo nodox = nodo;
        clsNodo derecha = nodo.getNodoHijoDerecho();

        while (derecha.getNodoHijoDerecho() != null) {
            nodox = derecha;
            derecha = derecha.getNodoHijoDerecho();
        }

        nodo.setPersona(derecha.getPersona());
        if (nodox == nodo) {
            nodox.setNodoHijoIzquierda(derecha.getNodoHijoIzquierda());
        } else {
            nodox.setNodoHijoDerecho(derecha.getNodoHijoIzquierda());
        }
        return derecha;
    }

    public void imprimir(String v) {
        JOptionPane.showMessageDialog(null, v);
    }
}
